#include "doctest.h"
#include <vector>
#include <algorithm>

void remove_duplicates(std::vector<int>& vec) {

}


SCENARIO("remove duplicate numbers") {

  GIVEN("a collection with some elements") {

    std::vector<int> vec = {1,1,2,2,2,3,3,4,5,5} ;

    WHEN("when removing duplicate entries") {

      remove_duplicates(vec) ;

      THEN("we have a collection with unique elements") {
        CHECK_EQ (vec, std::vector<int>{1,2,3,4,5}) ;
      }
    }
  }
}

