#include "doctest.h"
#include <vector>
#include <algorithm>

int the_smallest_element(const std::vector<int>& vec) {
  REQUIRE(vec.size() > 0); // ignore the problem of an empty vector
  
}


SCENARIO("get the smallest element") {

  GIVEN("a collection with some elements") {

    std::vector<int> vec = {5, 7, 2, 6, 8} ;

    WHEN("we are looking for the smallest element") {

      auto answer = the_smallest_element(vec) ;

      THEN("it was easy to find") {
        CHECK_EQ (answer, 2) ;
      }
    }

  }
}

