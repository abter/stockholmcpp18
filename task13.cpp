#include "doctest.h"
#include <string>
#include <algorithm>


bool is_substring(const std::string& str, const std::string& sub) {

}


SCENARIO("is sub string") {

  GIVEN("a string") {

    std::string str ="Hello SwedenCpp::Stockholm!" ;

    WHEN("we search for a substring") {

      std::string sub ="lo Sw" ;

      THEN("it is detected") {
        CHECK (is_substring(str, sub)) ;

      }
    }

    WHEN("we search for a different string") {

      std::string sub ="fub" ;

      THEN("it is not detected") {
        CHECK_FALSE (is_substring(str, sub)) ;

      }
    }
  }
}

