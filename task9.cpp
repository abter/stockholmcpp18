
#include "doctest.h"
#include <vector>

#include <numeric>

void from_0_to_10(std::vector<int>& vec) {

}


SCENARIO("put in numbers") {

  GIVEN("a collection with 10 elements") {

    std::vector<int> vec = {0,0,0,0,0,0,0,0,0,0} ;

    REQUIRE (vec.size() == 10);

    WHEN("we fill it  from_0_to_10") {

      from_0_to_10(vec) ;

      THEN("the elements are 0,1,2,3,4,5,6,7,8,9")
        CHECK_EQ (vec,
                  std::vector<int>{0,1,2,3,4,5,6,7,8,9}
        ) ;
    }
  }


}

